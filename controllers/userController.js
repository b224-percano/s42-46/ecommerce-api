const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



// Controller Function

// User Registration Function
module.exports.registerUser = async (reqBody) => {
    try {

        const findEmail =  await User.find({email: reqBody.email})
        console.log(findEmail)

        if(findEmail == false) {
            let newUser = new User({
                name: reqBody.name,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10)
            });
            await newUser.save()
            return "Congratulations! you Created an account" 
        } else {
            return "Account already exist with this email" 
        }
    } catch (error) {
        return "Error" 
    }
};


// Login Function
module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {
            return "Email does not exist" //false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            } else {
                return "Warning: Wrong Password" // false
            }
        }
    })
};


// User Details Function
module.exports.getAccounts = async (data) => {
    try {

        const accounts =  await User.findById(data.id)

        if(accounts){ 
            accounts.password = "**********"
            return accounts
        }else {
            return "You are not allowed to access this page" // false
        }
    }catch(error) {
        return "Error" //false
    }
};

