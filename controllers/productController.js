const Product = require("../models/Product.js");


// Product Controller Function

// Creating Product (Admin Only)
module.exports.createProduct = async (reqBody) => {
	try {
		
		const findUser =  await Product.find({name: reqBody.name})

		if(findUser == false){
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
				stock: reqBody.stock
			});
			await newProduct.save()
			return `Product ${newProduct.name} Added to shop`
		} else {
			return "This Product is already exist" 
		}

	} catch(error) {
		console.log(error)
		return "Error"
	}
};


// GET All Active/Inactive Product
module.exports.adminProducts = async () => {
	try {

		const adminProducts = await Product.find()
		if(adminProducts){
			return adminProducts
		}else {
			return false
		}

	}catch(error) {
		return "Error"
	}

};


// GET All Active Product
module.exports.getActiveProducts = async () => {
	try {

		const activeProduct = await Product.find({isActive: true})
		if(activeProduct){
			return activeProduct
		} else {
			return false
		}

	}catch(error) {
		return "Error"
	}

};


// GET Specific Product Information
module.exports.getProduct = async (reqParams) => {
	try {

		const singleProduct =  await Product.findById(reqParams.id)
		if(singleProduct){
			return singleProduct
		} else {
			return "Product does not exist"
		}

	}catch(error) {
		return "Error"
	}
};


// Update Product (Admin Only)
module.exports.updateProduct = async (reqParams, reqBody) => {
	try {

		let data = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			stock: reqBody.stock
		}
		const update = await Product.findByIdAndUpdate(reqParams.id, data)
		if(update){
			return `The changes in ${update.name} have been successfully saved!`
		} else{
			return "Updated Failed" 
		}

	}catch(error) {
		return "Error"
	}

};


// Archive Product (Admin Only)
module.exports.archiveProduct = async (reqParams) => {
	try {

		let data = {
			isActive: false
		}
		const update = await Product.findByIdAndUpdate(reqParams.id, data)
		if(update){
			return `The ${update.name} have been successfully archived!`
		} else {
			return "Product does not exist" 
		}

	}catch(error) {
		return "Error"
	}

};


// Activate Product (Admin Only)
module.exports.activateProduct = async (reqParams) => {
	try {

		let data = {
			isActive: true
		}
		const update = await Product.findByIdAndUpdate(reqParams.id, data)
		if(update){
			return `The ${update.name} have been successfully Activated!`
		}else {
			return "Product does not exist"
		}

	}catch(error) {
		return "Error"
	}

};

