const Order = require("../models/Order.js");
const Product = require("../models/Product.js");


// Controller Functions

// Order Product (User Only)
module.exports.createOrder = async (reqBody, data) => {
		try {

			const product = await Product.findById(reqBody.productId)
			let order = new Order ({
				userId: data.id,
				userName: data.name,
				products: [{
					productId: product.id,
					productName: product.name,
					quantity: reqBody.quantity || 1,
					price: product.price
				}],
				totalAmount: product.price * (reqBody.quantity || 1),
				address: reqBody.address

			});

			if(order) {
				let update = {
					stock: product.stock - (reqBody.quantity || 1)
				}
				if(update.stock <= 0){
					 update = {
						stock: 0,
						isActive: false
					}
				}
				await Product.findByIdAndUpdate(reqBody.productId, update)
				await order.save()
				return `${product.name} added to cart!`
			}else {
				return false
			}

	}catch(error) {

		return "Error"
	}
};


// GET Order Product
module.exports.orderList = async (data) => {
	try {

		const orders = await Order.find({userId: data.id})
		if(orders){
			return orders
		}else{
			return "Order does not exist"
		}

	}catch(error) {
		return "Error"
	}
};
