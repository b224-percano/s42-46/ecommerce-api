const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js');
const auth = require("../auth.js");


// Routes

// User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then((resultFromController) => {
		res.send(resultFromController)
	})
});


// User Details
router.get("/myAccount", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getAccounts(userData).then((resultFromController) => res.send(resultFromController))
});








module.exports = router;