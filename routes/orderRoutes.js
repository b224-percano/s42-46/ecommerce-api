const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const orderController = require("../controllers/orderController.js");

// Routes

// Order Product (User Only)
router.post("/createOrder", auth.verify, (req , res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == false) {
		orderController.createOrder(req.body, userData).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Only Users can add orders")
	}
});


// Order Information (User Only)
router.get("/orderList", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == false) { 
		orderController.orderList(userData).then((resultFromController) => res.send (resultFromController))
	}else {
		res.status(403).send("Only Users can view orders")
	}
});




module.exports = router;