const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productController = require("../controllers/productController.js");

// Routes

// GET all Active/Inactive Products
router.get("/adminProducts", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.adminProducts().then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Only Administrator can View all products")
	}
	
});


// GET all Active Products
router.get("/allProducts", (req, res) => {
	productController.getActiveProducts().then((resultFromController) => res.send(resultFromController))
});


// GET Specific Products
router.get("/find/:id", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send (resultFromController))
});


// Creating Product Route (Admin Only)
router.post("/createProduct", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.createProduct(req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Only Administrator can Create a product")
	}
});


// Update Product (Admin Only)
router.put("/update/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.updateProduct(req.params,req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Only Administrators can Update a product")
	}
});


// Archive Product (Admin Only)
router.patch("/archive/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.archiveProduct(req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Only Administrators can Archive a product")
	}
});


// Activate Product (Admin Only)
router.patch("/activate/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		productController.activateProduct(req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Only Administrators can Activate a product")
	}
});







module.exports = router;